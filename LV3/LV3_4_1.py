import pandas as pd
import matplotlib . pyplot as plt
data = pd.read_csv('LV3/data_C02_emission.csv')

# a)
print(len(data))
data.drop_duplicates()
data.dropna(axis=0)
data = data.reset_index(drop=True)

data['Make'] = data['Make'].astype('category')
data['Model'] = data['Model'].astype('category')
data['Vehicle Class'] = data['Vehicle Class'].astype('category')
data['Transmission'] = data['Make'] = data['Transmission'].astype('category')
data['Fuel Type'] = data['Fuel Type'].astype('category')

print(len(data))
print(data.info())

# b)
largest = data.nlargest(3, 'Fuel Consumption City (L/100km)')
smallest = data.nsmallest(3, 'Fuel Consumption City (L/100km)')

print(largest[['Make', 'Model', 'Fuel Consumption City (L/100km)']])
print(smallest[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

#c
engine_size=data.loc[(data['Engine Size (L)']<3.5)&(data['Engine Size (L)']>2.5)]
print(engine_size[['CO2 Emissions (g/km)']])
import pandas as pd
import matplotlib . pyplot as plt
data = pd.read_csv('LV3/data_C02_emission.csv')

# a)
plt.figure()
data['CO2 Emissions (g/km)'].plot(kind='hist', bins=20)
plt.show()

# b)
data.plot.scatter(x ='Fuel Consumption City (L/100km)', y ='CO2 Emissions (g/km)', c='Fuel Type', cmap='hot')
plt.show()

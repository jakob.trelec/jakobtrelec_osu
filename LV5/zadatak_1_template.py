import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn . metrics import accuracy_score
from sklearn . metrics import confusion_matrix , ConfusionMatrixDisplay

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

plt.figure()
plt.scatter(X_train[:,0], X_train[:,1],c=y_train,cmap='RdBu',s=3)
plt.scatter(X_test[:,0], X_test[:,1],c=y_test,cmap='PRGn',s=9, marker="x")
plt.show()

# inicijalizacija i ucenje modela logisticke regresije
LogRegression_model = LogisticRegression ()
LogRegression_model . fit ( X_train , y_train )

# Dobivanje koeficijenata modela
theta0 = LogRegression_model.intercept_[0]
theta1, theta2 = LogRegression_model.coef_[0]

# Izračun granice odluke
x1_values = np.linspace(np.min(X[:, 0]), np.max(X[:, 0]), 100)
x2_values = -(theta0 + theta1 * x1_values) / theta2

plt.figure()
plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap='coolwarm', marker='o', label='Train Data')
plt.plot(x1_values, x2_values, 'r-')
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()
# predikcija na skupu podataka za testiranje
y_test_predict = LogRegression_model . predict ( X_test )

print (" Tocnost : " , accuracy_score (y_test , y_test_predict ))
# matrica zabune
cm = confusion_matrix ( y_test , y_test_predict )
print (" Matrica zabune : " , cm)
disp = ConfusionMatrixDisplay ( confusion_matrix (y_test , y_test_predict ))
disp . plot ()
plt . show ()
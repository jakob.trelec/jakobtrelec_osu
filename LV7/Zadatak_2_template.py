import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("LV7/imgs/test_1.jpg")
img1= Image.imread("LV7/imgs/test_2.jpg")
img2= Image.imread("LV7/imgs/test_3.jpg")
img3= Image.imread("LV7/imgs/test_4.jpg")
img4= Image.imread("LV7/imgs/test_5.jpg")
img5= Image.imread("LV7/imgs/test_6.jpg")


############################################
#6
# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika

K=[1,2,3,4,5,6,7,8]
J=[]

for i in K:
    img_array_aprox = img_array.copy()
    km = KMeans ( n_clusters = i , init ="random",
    n_init =5 , random_state =0 )
    km.fit(img_array_aprox)
    J.append(km.inertia_)

    labels=km.predict(img_array_aprox)
    for i in range(len(labels)):
        img_array_aprox[i]=km.cluster_centers_[labels[i]]
    
plt.figure()
plt.title("promjenjena")
plt.imshow(img_array_aprox.reshape(w,h,d))
plt.tight_layout()
plt.show()

plt.figure()
plt.title("Ovisnost J o K")
plt.scatter(K,J)
plt.show()


# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img1)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img1 = img1.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img1.shape
img_array = np.reshape(img1, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()
km = KMeans ( n_clusters = 8 , init ="random",
n_init =5 , random_state =0 )
km.fit(img_array_aprox)
labels=km.predict(img_array_aprox)
for i in range(len(labels)):
    img_array_aprox[i]=km.cluster_centers_[labels[i]]
plt.figure()
plt.title("promjenjena")
plt.imshow(img_array_aprox.reshape(w,h,d))
plt.tight_layout()
plt.show()

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img2)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img2 = img2.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img2.shape
img_array = np.reshape(img2, (w*h, d))
img_array_aprox = img_array.copy()
km = KMeans ( n_clusters = 8 , init ="random",
n_init =5 , random_state =0 )
km.fit(img_array_aprox)
labels=km.predict(img_array_aprox)
for i in range(len(labels)):
    img_array_aprox[i]=km.cluster_centers_[labels[i]]
plt.figure()
plt.title("promjenjena")
plt.imshow(img_array_aprox.reshape(w,h,d))
plt.tight_layout()
plt.show()

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img3)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img3 = img3.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img3.shape
img_array = np.reshape(img3, (w*h, d))
img_array_aprox = img_array.copy()
km = KMeans ( n_clusters = 8 , init ="random",
n_init =5 , random_state =0 )
km.fit(img_array_aprox)
labels=km.predict(img_array_aprox)
for i in range(len(labels)):
    img_array_aprox[i]=km.cluster_centers_[labels[i]]
plt.figure()
plt.title("promjenjena")
plt.imshow(img_array_aprox.reshape(w,h,d))
plt.tight_layout()
plt.show()

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img4)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img4 = img4.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img4.shape
img_array = np.reshape(img4, (w*h, d))
img_array_aprox = img_array.copy()
km = KMeans ( n_clusters = 8 , init ="random",
n_init =5 , random_state =0 )
km.fit(img_array_aprox)
labels=km.predict(img_array_aprox)
for i in range(len(labels)):
    img_array_aprox[i]=km.cluster_centers_[labels[i]]
plt.figure()
plt.title("promjenjena")
plt.imshow(img_array_aprox.reshape(w,h,d))
plt.tight_layout()
plt.show()

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img5)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img5 = img5.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img5.shape
img_array = np.reshape(img5, (w*h, d))
img_array_aprox = img_array.copy()
km = KMeans ( n_clusters = 8 , init ="random",
n_init =5 , random_state =0 )
km.fit(img_array_aprox)
labels=km.predict(img_array_aprox)
for i in range(len(labels)):
    img_array_aprox[i]=km.cluster_centers_[labels[i]]
plt.figure()
plt.title("promjenjena")
plt.imshow(img_array_aprox.reshape(w,h,d))
plt.tight_layout()
plt.show()


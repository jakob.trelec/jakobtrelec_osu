import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster.hierarchy import dendrogram
from sklearn.datasets import make_blobs, make_circles, make_moons
from sklearn.cluster import KMeans, AgglomerativeClustering


def generate_data(n_samples, flagc):
    # 3 grupe
    if flagc == 1:
        random_state = 365
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
    
    # 3 grupe
    elif flagc == 2:
        random_state = 148
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    # 4 grupe 
    elif flagc == 3:
        random_state = 148
        X, y = make_blobs(n_samples=n_samples,
                        centers = 4,
                        cluster_std=np.array([1.0, 2.5, 0.5, 3.0]),
                        random_state=random_state)
    # 2 grupe
    elif flagc == 4:
        X, y = make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
    # 2 grupe  
    elif flagc == 5:
        X, y = make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

# generiranje podatkovnih primjera
for i in range(3,6):
    km = KMeans ( n_clusters =i , init ="random",
    n_init =5 , random_state =0 )

    X = generate_data(500, 1)
    km.fit(X)
    labels=km.predict(X)
    print(labels)
    plt.figure()
    plt.scatter(X[:,0],X[:,1],c=labels)
    plt.show()

    X1= generate_data(500, 2)
    km.fit(X1)
    labels=km.predict(X1)
    print(labels)
    plt.figure()
    plt.scatter(X1[:,0],X1[:,1],c=labels)
    plt.show()

    X2=generate_data(500, 3)
    km.fit(X2)
    labels=km.predict(X2)
    print(labels)
    plt.figure()
    plt.scatter(X2[:,0],X2[:,1],c=labels)
    plt.show()

    X3=generate_data(500, 4)
    km.fit(X3)
    labels=km.predict(X3)
    print(labels)
    plt.figure()
    plt.scatter(X3[:,0],X3[:,1],c=labels)
    plt.show()

    X4=generate_data(500, 5)
    km.fit(X4)
    labels=km.predict(X4)
    print(labels)
    plt.figure()
    plt.scatter(X4[:,0],X4[:,1],c=labels)
    plt.show()
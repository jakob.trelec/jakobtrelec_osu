print("uPISI OCJENU")

try:
    mark = input()
    if mark > 1:
        raise ValueError
    if mark<0:
        raise ValueError
    if mark == str:
        raise TypeError
except ValueError:
    print("Niste unijeli broj u rasponu")
except TypeError:
    print("Nisi unio broj")


else:
    if float(mark)>=0.9:
        print("A")
    elif float(mark)>=0.8:
        print("B")
    elif float(mark)>=0.7:
        print("C")
    elif float(mark)>=0.6:
        print("D")
    else:
        print("F")
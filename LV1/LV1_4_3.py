is_done=0
numbers_list=[]

while is_done==0:
    try:
        unos=input()
        if unos=='Done':
            is_done=1
        if unos == str:
            raise ValueError
        else:
            numbers_list.append(float(unos))

    except ValueError:
        print("Nisi unio broj")

numbers_list.sort()
print(numbers_list)
print("Broj brojeva")
print(len(numbers_list))
print("Srednja vrijednost")
print(sum(numbers_list)/len(numbers_list))
print("Maksimalna i minimalna vrijednost")
print(max(numbers_list),"i",min(numbers_list))
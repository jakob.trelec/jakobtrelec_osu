import numpy as np
import matplotlib . pyplot as plt
import csv

data=np.loadtxt('LV2\data.csv',delimiter=',', skiprows=1)

#a
num_of_people=data.shape[0]
print(num_of_people)

#b
plt.scatter(data[:,1],data[:,2],c=data[0::,0],cmap='RdBu')
plt.ylabel('weight')
plt.xlabel('height')
plt.title('data')
plt.show()

#c
plt.scatter(data[0::50,1],data[0::50,2],c=data[0::50,0],cmap='PiYG')
plt.ylabel('weight')
plt.xlabel('height')
plt.title('data')
plt.show()

#d
print(np.max(data[0::,1]))
print(np.min(data[0::,1]))
print(np.mean(data[0::,1]))

#e
muskarci=(data[:,0]==1)
zene=(data)
import numpy as np
import matplotlib.pyplot as plt

data =  np.loadtxt("data.csv", delimiter=',', skiprows=1)
#print(data)

print("Number od samples: " + str(len(data)))

plt.figure()
plt.scatter(data[:, 1], data[:, 2], s=0.5)
plt.title("Height vs. weight")
plt.show()

plt.figure()
plt.scatter(data[::50, 1], data[::50, 2], s=0.5)
plt.title("Height vs. weight every 50 sample")
plt.show()

print("Min height: " + str(np.min(data[:, 1])))
print("Max height: " + str(np.max(data[:, 1])))
print("Mean height: " + str(np.mean(data[:, 1])))

print("Man")
m = (data[:,0] == 1)
print("Min height: " + str(np.min(data[m, 1])))
print("Max height: " + str(np.max(data[m, 1])))
print("Mean height: " + str(np.mean(data[m, 1])))

print("Woman")
f = (data[:,0] == 0)
print("Min height: " + str(np.min(data[f, 1])))
print("Max height: " + str(np.max(data[f, 1])))
print("Mean height: " + str(np.mean(data[f, 1])))
import numpy as np
import matplotlib . pyplot as plt

img = plt.imread ("road.jpg")
img = img[ :,:,0].copy()

print(img.shape)

light_img = img[:, :]
plt.figure()
plt.imshow( light_img , cmap ="gray", alpha=0.5)

_1_4_img = img[:, round(len(img[:, 0])/3) : round(len(img[:, 0])/3*2)]
plt.figure()
plt.imshow( _1_4_img , cmap ="gray")

_90_rotate_img = np.rot90(img, k=3)
plt.figure()
plt.imshow( _90_rotate_img , cmap ="gray")

mirror_img = np.flip(img, axis=1)
plt.figure()
plt.imshow( mirror_img , cmap ="gray")

plt.show()
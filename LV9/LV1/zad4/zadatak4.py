f = "song.txt"
f = open(f, "r")

words = []

for line in f:
    line = line.split()
    for w in line:
        if w.endswith(','):
            words.append(w[0:len(w)-1].lower())
        else:
            words.append(w.lower())


dicitionary = {}

for w in words:
    dicitionary[w] = words.count(w)

for w in dicitionary:
    if dicitionary[w] == 1:
        print("Word: "+ str(w) + "   Count: " + str(dicitionary[w]))
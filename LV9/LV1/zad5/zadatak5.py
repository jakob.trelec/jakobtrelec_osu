f = "SMSSpamCollection.txt"
f = open(f, "r")

messages = []

for line in f:
    messages.append(line.strip())

numOfSpams = 0
avgWordsSpam = 0
spamEndingQuestion = 0
for message in messages:
    if message.startswith('s'):
        numOfSpams += 1
        line=[]
        line = message.split()
        avgWordsSpam += len(line) - 1
        if message.endswith('!'):
            spamEndingQuestion += 1
avgWordsSpam/= numOfSpams

numOfHams = 0
avgWordsHam = 0
for message in messages:
    if message.startswith('h'):
        numOfHams += 1
        line=[]
        line = message.split()
        avgWordsHam += len(line) - 1
avgWordsHam /= numOfHams

print(f"Number of spam messages: {numOfSpams}, average number of words in spam messages: {avgWordsSpam}")
print(f"Number of ham messages: {numOfHams}, average number of words in ham messages: {avgWordsHam}")
print(f"Number of spam messages ending with '!': {spamEndingQuestion}")

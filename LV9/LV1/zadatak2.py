try:
    while True:
        num = float(input("Enter number: "))
        if num > 1 or num < 0:
            print("Number not in [0.0, 1.0] range.")
        else:
            break
    if num >= 0.9:
        print("A")
    elif num >= 0.8:
        print("B")
    elif num >= 0.7:
        print("C")
    elif num >= 0.6:
        print("D")
    else:
        print("F")
except:
    print("Not a number.")
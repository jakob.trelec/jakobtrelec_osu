numbers = []

while True:
    n = input("Enter number: ").lower()
    if n.isdigit():
        n = float(n)
        numbers.append(n)
    elif n.isdigit() == False:
        if n.casefold() == "done".casefold():
            print("Done")
            break
        else:
            print("Not a number!")


print("Number of digits entered: " + str(len(numbers)))
print("Average: " + str(sum(numbers)/len(numbers)))
print("Max value: " + str(max(numbers)))
print("Min value: " + str(min(numbers)))
numbers = sorted(numbers)
print(numbers)
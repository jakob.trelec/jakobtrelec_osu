import numpy as n
import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('data_C02_emission.csv')
data['Transmission'] = data['Transmission'].astype('category')
data['Fuel Type'] = data['Fuel Type'].astype('category')
data['Make'] = data['Make'].astype('category')
data['Model'] = data['Model'].astype('category')
data['Vehicle Class'] = data['Vehicle Class'].astype('category')
print(f"Number of elements: {len(data)}")
print(f"Types of data: {data.dtypes}")

print("\nThree cars with most fuel consumption city:")
print(data.sort_values(by="Fuel Consumption City (L/100km)", ascending=False)[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3))
print("\nThree cars with least fuel consumption city:")
print(data.sort_values(by="Fuel Consumption City (L/100km)", ascending=False)[['Make', 'Model', 'Fuel Consumption City (L/100km)']].tail(3))

engine_size_data = data[( data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5 )]
print(f"\nNumber of cars with engin size >= 2.5 and <= 3.5: {len(engine_size_data)}")
print(f"Average CO2 emmision for those cars: {engine_size_data[['CO2 Emissions (g/km)']].mean()}")

audi_cars = data[(data['Make'] == "Audi")]
print(f"\nNumber of Audi cars: {len(audi_cars)}")
print(f"Average CO2 emmision of Audi cars with 4 cylinders: {audi_cars[(audi_cars['Cylinders'] == 4)][['CO2 Emissions (g/km)']].mean()}")

cylinders_data = data.groupby('Cylinders')
print("\nNumber of cars depending on number od cylinders: ")
print(cylinders_data['Cylinders'].count())
print("Average CO2 emmision depending on number of cylinders: ")
print(cylinders_data['CO2 Emissions (g/km)'].mean())

disel_cars = data[(data['Fuel Type'] == "D")]
print(f"\nAverage city fuel consumption for disel cars: {disel_cars['Fuel Consumption City (L/100km)'].mean()}")
print(f"Median city fuel consumption for disel cars: {disel_cars['Fuel Consumption City (L/100km)'].median()}")
regular_gasoline_cars = data[(data['Fuel Type'] == "X")]
print(f"Average city fuel consumption for regular gasoline cars: {regular_gasoline_cars['Fuel Consumption City (L/100km)'].mean()}")
print(f"Median city fuel consumption for disel cars: {regular_gasoline_cars['Fuel Consumption City (L/100km)'].median()}")

disel_cars = disel_cars[disel_cars['Cylinders'] == 4]
print(f"\nFour(4) cylinder disel car with max city fuel consumption:\n {disel_cars[disel_cars['Fuel Consumption City (L/100km)'] == disel_cars['Fuel Consumption City (L/100km)'].max()]}")

manual_cars = data[data['Transmission'].str.startswith('M')]
print(f"Number of manual cars: {len(manual_cars)}")

print(f"\nCorrelation between numeric data: {data.corr(numeric_only=True)}")
import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')
data['Transmission'] = data['Transmission'].astype('category')
data['Fuel Type'] = data['Fuel Type'].astype('category')
data['Make'] = data['Make'].astype('category')
data['Model'] = data['Model'].astype('category')
data['Vehicle Class'] = data['Vehicle Class'].astype('category')

plt.figure()
data['CO2 Emissions (g/km)'].plot( kind ='hist', bins = 20 )
plt.title("Hist for CO2 emmision")
plt.xlabel("CO2 emmision")
plt.show()

colors = {'X':'red', 'Z':'green', 'D':'blue', 'E':'magenta', 'N':'yellow'}
data.plot.scatter( x='Fuel Consumption City (L/100km)' ,
                   y='CO2 Emissions (g/km)',    
                   c=data['Fuel Type'].map(colors), s=5)
plt.title("CO2 emmision/Fuel consumption city by Fuel type")
plt.xlabel("Fuel Consumption city")
plt.ylabel("CO2 emmision")
plt.show()

data.boxplot(by='Fuel Type', column=['Fuel Consumption Hwy (L/100km)'])
plt.show()

plt.figure()
grouped_fuel = data.groupby('Fuel Type')
grouped_fuel['Model'].count().plot(kind='bar')
plt.title("Number of cars by fuel type")
plt.ylabel("Number of cars")
plt.xlabel("Fuel type")
plt.show()

plt.figure()
grouped_cylinders = data.groupby('Cylinders')
grouped_cylinders['CO2 Emissions (g/km)'].mean().plot(kind='bar')
plt.title("Average CO2 emmision by number od cylinders")
plt.xlabel("Number of cylinders")
plt.ylabel("Average CO2 emmision")
plt.show()
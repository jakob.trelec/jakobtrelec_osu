from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error, r2_score, max_error
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
from sklearn . preprocessing import OneHotEncoder

data = pd.read_csv('data_C02_emission.csv')

input = ['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 
          'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)', 'Fuel Type']
output = ['CO2 Emissions (g/km)']

ohe = OneHotEncoder()
X_encoded = ohe.fit_transform(data[['Fuel Type']]).toarray()
data['Fuel Type'] = X_encoded
X = data[input].to_numpy()
y = data[output].to_numpy()

X_train , X_test , y_train , y_test = train_test_split(X , y , test_size = 0.2 , random_state =1)

linearModel = lm.LinearRegression()
linearModel.fit( X_train , y_train )
print("Model coef: ")
print(linearModel.coef_)

y_test_p = linearModel.predict( X_test )

MSE = mean_squared_error(y_test, y_test_p)
print("Mean squared error: " + str(MSE))
RMSE = math.sqrt(MSE)
print("Root mean squared error: " + str(RMSE))
MAE = mean_absolute_error( y_test , y_test_p )
print("Mean absolute error: " + str(MAE))
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
print("Mean absolute precentage error: " + str(MAPE))
r = r2_score(y_test, y_test_p)
print("R2: " + str(r))

mistake = abs(y_test - y_test_p)
ind = np.argmax(mistake)
print("Car with max error CO2 emmision: ")
print(data.iloc[ind])
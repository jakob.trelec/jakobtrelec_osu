from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error, r2_score
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math

data = pd.read_csv('data_C02_emission.csv')

X = data[['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 
              'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)']]
y = data[['CO2 Emissions (g/km)']]

X_train , X_test , y_train , y_test = train_test_split(X , y , test_size = 0.2 , random_state =1)

plt.figure()
plt.scatter(X_train['Fuel Consumption City (L/100km)'], y_train['CO2 Emissions (g/km)'], c='blue', s=5)
plt.scatter(X_test['Fuel Consumption City (L/100km)'], y_test['CO2 Emissions (g/km)'], c='red', s=5)
plt.legend(["Train", "Test"])
plt.title("CO2 emmision depending on Fuel Consumption City")
plt.xlabel("Fuel consumption city")
plt.ylabel("CO2 emmision")
plt.show() 

sc = StandardScaler()
X_train_n = sc.fit_transform( X_train )
X_test_n = sc.transform( X_test )

plt.figure()
plt.hist(X_train['Fuel Consumption City (L/100km)'], bins=20, alpha=0.5, label='x', color='magenta')
plt.hist(X_train_n[:, 2:3], bins=20, alpha=0.5, label='y', color='green')
plt.title("Histogram of Fuel consumption city with and without transformation")
plt.xlabel("Fuel Consumption City(transformed and not transformed)")
plt.ylabel("Frequency")
plt.show()

linearModel = lm.LinearRegression()
linearModel.fit( X_train_n , y_train )
print("Model coef: ")
print(linearModel.coef_)

y_test_p = linearModel.predict( X_test_n )
plt.figure()
plt.scatter(y_test['CO2 Emissions (g/km)'], y_test_p, c='magenta', s=5)
plt.title("True vs predicted CO2 emission for test base")
plt.xlabel("Test CO2 emission")
plt.ylabel("Predicted CO2 emission")
plt.show() 

MSE = mean_squared_error(y_test, y_test_p)
print("Mean squared error: " + str(MSE))
RMSE = math.sqrt(MSE)
print("Root mean squared error: " + str(RMSE))
MAE = mean_absolute_error( y_test , y_test_p )
print("Mean absolute error: " + str(MAE))
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
print("Mean absolute precentage error: " + str(MAPE))
r = r2_score(y_test, y_test_p)
print("R2: " + str(r))

print("\nPrediction with smaller train database")
X_train , X_test , y_train , y_test = train_test_split(X , y , test_size = 0.3 , random_state =1)
sc = StandardScaler()
X_train_n = sc.fit_transform( X_train )
X_test_n = sc.transform( X_test )
linearModel = lm.LinearRegression()
linearModel.fit( X_train_n , y_train )
print("Model coef: ")
print(linearModel.coef_)
y_test_p = linearModel.predict( X_test_n )
MSE = mean_squared_error(y_test, y_test_p)
print("Mean squared error: " + str(MSE))
RMSE = math.sqrt(MSE)
print("Root mean squared error: " + str(RMSE))
MAE = mean_absolute_error( y_test , y_test_p )
print("Mean absolute error: " + str(MAE))
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
print("Mean absolute precentage error: " + str(MAPE))
r = r2_score(y_test, y_test_p)
print("R2: " + str(r))
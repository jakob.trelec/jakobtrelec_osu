import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, confusion_matrix , ConfusionMatrixDisplay, precision_score, recall_score


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

plt.figure()
cmap = ListedColormap([[0,0,1], [1,0,0]])
plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cmap, s=10)
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cmap, s=10, marker='x')
plt.legend(['0', '1'])
plt.xlabel("X1")
plt.ylabel("X2")
plt.title("Train(.) and Test(x) data x1 vs x2")
plt.show() 

LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train , y_train)
y_test_p = LogRegression_model.predict(X_test)

print("Models coefs: " + str(LogRegression_model.coef_))

b = LogRegression_model.intercept_[0]
w1, w2 = LogRegression_model.coef_.T
# Calculate the intercept and gradient of the decision boundary.
c = -b/w2
m = -w1/w2
plt.figure()
cmap = ListedColormap([[1,0,0], [0,0,1]])
plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cmap, s=10)
plt.xlabel("X1")
plt.ylabel("X2")
xmin, xmax = -5, 6
ymin, ymax = -5, 6
xd = np.array([xmin, xmax])
yd = m*xd + c
plt.plot(xd, yd, 'k', lw=1, ls='--')
plt.fill_between(xd, yd, ymin, color='tab:blue', alpha=0.2)
plt.fill_between(xd, yd, ymax, color='tab:orange', alpha=0.2)
plt.title("Model line")
plt.show() 

print ("Accuracy : " , accuracy_score(y_test , y_test_p))
print ("Precision : " , precision_score(y_test , y_test_p))
print ("Recall : " , recall_score(y_test , y_test_p))
cm = confusion_matrix(y_test , y_test_p)
print ("Confusion matrix : " , cm )
disp = ConfusionMatrixDisplay(confusion_matrix(y_test , y_test_p))
disp.plot()
plt.show()

plt.figure()
cmap = ListedColormap([[0,1,0], [0,0,0]])
plt.scatter(X_test[:, 0], X_test[:, 1], c=(y_test ^ y_test_p), cmap=cmap, s=10)
plt.xlabel("X1")
plt.ylabel("X2")
plt.title("Correct vs incorrect prediction")
plt.show() 
from sklearn import datasets
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler
import sklearn . linear_model as lm
from sklearn . metrics import mean_absolute_error
import pandas as pd
import matplotlib . pyplot as plt
import numpy

data = pd.read_csv('LV3/data_C02_emission.csv')
data.drop_duplicates()
data.dropna(axis=0)

X=data[['Engine Size (L)','Cylinders','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']]
y=data[['CO2 Emissions (g/km)']]

#a
X_train , X_test , y_train , y_test = train_test_split (X , y , test_size = 0.2 , random_state = 1)

#b
plt.figure()
plt.scatter(X_train['Fuel Consumption Comb (L/100km)'], y_train,cmap='Blue',s=3)
plt.scatter(X_test['Fuel Consumption Comb (L/100km)'], y_test,cmap='Red',s=3)
plt.show()
#c
sc = MinMaxScaler ()
X_train_n = pd.DataFrame(sc.fit_transform ( X_train ))

plt.hist(X_train['Fuel Consumption Comb (L/100km)'])
plt.show()

plt.hist(X_train_n[4])
plt.show()

X_test_n = sc.transform ( X_test )

#d
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n , y_train)
print(linearModel.coef_)

#e
y_test_p = linearModel.predict(X_test_n)
plt.scatter(X_test_n[:,2], y_test,c='Green',s=4)
plt.scatter(X_test_n[:,2], y_test_p, c='Yellow',s=4)
plt.show()
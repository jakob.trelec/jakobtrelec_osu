import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sklearn . linear_model as lm
import seaborn as sns
import math
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error, r2_score, max_error
from sklearn.metrics import confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay
from matplotlib.colors import ListedColormap


https://www.kaggle.com/code/nikhilvagala/wine-quality-analysis#Logistic-Regression
NA OVOM LINKU ^^^^^^^^^^ SU TI SVI DATASETI KOJE TI ZADA I MOS NAC BAR NEST ZAPOCETO